/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author test1
 */
public class Lab4 {
    
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}} ;
    
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int count = 0 ;
        
        Game.printWelcome();
        Table.printTable(table);
        while(true){
            Player.turnO();
            Table.printTable(table);
            if (Game.checkWinO(table) == true){ System.out.println("O win!"); break; }
            count++ ;
            if (Game.draw(count)) {System.out.println("Draw!"); break;}
            Player.turnX();
            Table.printTable(table);
            if (Game.checkWinX(table) == true){ System.out.println("X win!"); break ;}
            count++ ;
            if (Game.draw(count)) {System.out.println("Draw!"); break;}
            
        }

    }



}
